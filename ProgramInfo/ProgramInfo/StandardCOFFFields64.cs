﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class StandardCOFFFields64
    {
        public MagicType Magic;
        public byte MajorLinkerVersion;
        public byte MinorLinkerVersion;
        public uint SizeOfCode;
        public uint SizeOfInitializedData;
        public uint SizeOfUninitializedData;
        public uint AddressOfEntryPoint;
        public uint BaseOfCode;

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}
