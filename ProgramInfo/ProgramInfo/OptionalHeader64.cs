﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    public class OptionalHeader64
    {
        public StandardCOFFFields64 StandardCOFFFields { get; }
        public WindowsSpecificFields64 WindowsSpecificFields { get; }
        public DataDirectories DataDirectories { get; }
        
        public OptionalHeader64(IntPtr optionalHeaderAddress)
        {
            StandardCOFFFields = Marshal.PtrToStructure<StandardCOFFFields64>(optionalHeaderAddress);

            int sizeOfStandardCOFFFields = Marshal.SizeOf<StandardCOFFFields64>();
            var WindowsSpecificFieldsAddress = IntPtr.Add(optionalHeaderAddress, sizeOfStandardCOFFFields);
            WindowsSpecificFields = Marshal.PtrToStructure<WindowsSpecificFields64>(WindowsSpecificFieldsAddress);

            int sizeOfStandardWindowsSpecificFields = Marshal.SizeOf<WindowsSpecificFields64>();
            var COFFHeaderAddress = IntPtr.Add(WindowsSpecificFieldsAddress, sizeOfStandardWindowsSpecificFields);
            DataDirectories = Marshal.PtrToStructure<DataDirectories>(COFFHeaderAddress);
        }

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}
