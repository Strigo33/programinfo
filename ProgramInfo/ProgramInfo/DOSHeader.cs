﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DOSHeader
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public char[] MagicNumber;
        public ushort LastSize;
        public ushort PagesInFile;
        public ushort Relocations;
        public ushort HeadersSizeInParagraph;
        public ushort MinExtraParagraphNeeded;
        public ushort MaxExtraParagraphNeeded;
        public ushort InitialSSValue;
        public ushort InitialSPValue;
        public ushort Checksum;
        public ushort InitialIPValue;
        public ushort InitialCSValue;
        public ushort FileAddOfRealocTable;
        public ushort OverlayNumber;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public ushort[] ReservedWords1;
        public ushort OEMIdentifer;
        public ushort OEMInformation;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] ReservedWords2;
        public int PEHeaderAddress;

        public string MZ => new string(MagicNumber).TrimEnd('\0');

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}
