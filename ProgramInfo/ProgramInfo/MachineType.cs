﻿
namespace PE
{
    public enum MachineType : ushort
    {
        Native = 0,
        I386 = 0x014c,
        IA64 = 0x0200,
        AMD64 = 0x8664
    }
}
