﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public struct COFFHeader
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public char[] Signature;
        public MachineType Machine;
        public ushort NumberOfSections;
        public uint TimeDateStamp;
        public uint PointerToSymbolTable;
        public uint NumberOfSymbolTable;
        public ushort SizeOfOptionalHeader;
        public ushort Characteristics;

        public string SignatureString => new string(Signature).TrimEnd('\0');

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}
