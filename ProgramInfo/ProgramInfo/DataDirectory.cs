﻿using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class DataDirectory
    {
        public uint VirtualAddress;
        public uint Size;
    }
}
