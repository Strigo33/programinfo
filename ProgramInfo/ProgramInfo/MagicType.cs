﻿
namespace PE
{
    public enum MagicType : ushort
    {
        PE32 = 0x10b,
        PE64 = 0x20b
    }
}
