﻿using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class DataDirectories
    {
        public DataDirectory ExportTable;
        public DataDirectory ImportTable;
        public DataDirectory ResourceTable;
        public DataDirectory ExceptionTable;
        public DataDirectory CertificateTable;
        public DataDirectory BaseRelocationTable;
        public DataDirectory Debug;
        public DataDirectory Architecture;
        public DataDirectory GlobalPtr;
        public DataDirectory TLSTable;
        public DataDirectory LoadConfigTable;
        public DataDirectory BoundImport;
        public DataDirectory IAT;
        public DataDirectory DelayImportDescriptor;
        public DataDirectory CLRRuntimeHeader;
        public DataDirectory Reserved;
    }
}
