﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class OptionalHeader32
    {
        public StandardCOFFFields32 StandardCOFFFields;
        public WindowsSpecificFields32 WindowsSpecificFields;
        public DataDirectories DataDirectories;

        public OptionalHeader32(IntPtr optionalHeaderAddress)
        {
            StandardCOFFFields = Marshal.PtrToStructure<StandardCOFFFields32>(optionalHeaderAddress);

            int sizeOfStandardCOFFFields = Marshal.SizeOf<StandardCOFFFields32>();
            var WindowsSpecificFieldsAddress = IntPtr.Add(optionalHeaderAddress, sizeOfStandardCOFFFields);
            WindowsSpecificFields = Marshal.PtrToStructure<WindowsSpecificFields32>(WindowsSpecificFieldsAddress);

            int sizeOfStandardWindowsSpecificFields = Marshal.SizeOf<WindowsSpecificFields32>();
            var COFFHeaderAddress = IntPtr.Add(WindowsSpecificFieldsAddress, sizeOfStandardWindowsSpecificFields);
            DataDirectories = Marshal.PtrToStructure<DataDirectories>(COFFHeaderAddress);
        }

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}
