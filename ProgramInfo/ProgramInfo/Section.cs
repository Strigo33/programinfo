﻿using System;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class Section
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public char[] Name;        
        public uint VirtualSize;        
        public uint VirtualAddress;
        public uint SizeOfRawData;        
        public uint PointerToRawData;
        public uint PointerToRelocations;        
        public uint PointerToLinenumbers;
        public ushort NumberOfRelocations;        
        public ushort NumberOfLinenumbers;        
        public DataSectionFlags Characteristics;

        public string SectionName => new string(Name).TrimEnd('\0');

        public override string ToString()
        {
            return SectionName;
        }

        public byte[] GetBytes()
        {
            byte[] res = new byte[Marshal.SizeOf(this)];

            IntPtr ptr = Marshal.AllocHGlobal(res.Length);
            Marshal.StructureToPtr(this, ptr, true);
            Marshal.Copy(ptr, res, 0, res.Length);
            Marshal.FreeHGlobal(ptr);
            return res;
        }
    }
}