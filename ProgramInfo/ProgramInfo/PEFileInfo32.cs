﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace PE
{
    [StructLayout(LayoutKind.Sequential)]
    public class PEFileInfo32
    {
        public DOSHeader DOSHeader;
        public byte[] DOSStub;
        public COFFHeader COFFHeader;
        public OptionalHeader32 OptionalHeader;
        public Section[] SectionTable;
        public byte[][] SectionRawData;

        public PEFileInfo32(string path)
            : this(File.ReadAllBytes(path))
        {
        }

        public PEFileInfo32(byte[] data)
        {
            var handle = GCHandle.Alloc(data, GCHandleType.Pinned);
            var DOSHeaderAddress = handle.AddrOfPinnedObject();
            DOSHeader = Marshal.PtrToStructure<DOSHeader>(DOSHeaderAddress);

            int sizeOfDOSHeader = Marshal.SizeOf<DOSHeader>();
            DOSStub = new byte[DOSHeader.PEHeaderAddress - sizeOfDOSHeader];
            Array.Copy(data, sizeOfDOSHeader, DOSStub, 0, DOSStub.Length);

            var COFFHeaderAddress = IntPtr.Add(DOSHeaderAddress, DOSHeader.PEHeaderAddress);
            COFFHeader = Marshal.PtrToStructure<COFFHeader>(COFFHeaderAddress);

            if (COFFHeader.Machine == MachineType.AMD64)
                throw new FormatException("Data are of type AMD64. You should use PEFileInfo64");

            int sizeOfCOFFHeader = Marshal.SizeOf<COFFHeader>();
            var optionaleaderAddress = IntPtr.Add(COFFHeaderAddress, sizeOfCOFFHeader);
            OptionalHeader = new OptionalHeader32(optionaleaderAddress);

            int sizeOfSection = Marshal.SizeOf<Section>();
            var SectionTableAddress = IntPtr.Add(COFFHeaderAddress, sizeOfCOFFHeader + COFFHeader.SizeOfOptionalHeader);
            SectionTable = new Section[COFFHeader.NumberOfSections];
            SectionRawData = new byte[COFFHeader.NumberOfSections][];
            for (int i = 0; i < SectionTable.Length; i++)
            {
                var CurrentSectionTableAddress = IntPtr.Add(SectionTableAddress, i * sizeOfSection);
                SectionTable[i] = Marshal.PtrToStructure<Section>(CurrentSectionTableAddress);
                var CurrentSectionRawDataAddress = IntPtr.Add(DOSHeaderAddress, (int)SectionTable[i].PointerToRawData);
                SectionRawData[i] = new byte[SectionTable[i].SizeOfRawData];
                Array.Copy(data, SectionTable[i].PointerToRawData, SectionRawData[i], 0, SectionTable[i].SizeOfRawData);
            }
            handle.Free();
        }

        public long Size => 
            OptionalHeader.WindowsSpecificFields.SizeOfHeaders
            + SectionTable.Sum(s => s.SizeOfRawData);

        public byte[] GetBytes()
        {
            var res = new byte[Size];

            int sectionLength = Marshal.SizeOf<Section>();
            byte[] DOSHeaderData = DOSHeader.GetBytes();
            byte[] COFFHeaderData = COFFHeader.GetBytes();
            byte[] OptionalHeaderData = OptionalHeader.GetBytes();
            byte[] sectionsData = new byte[SectionTable.Length * sectionLength];

            Array.Copy(DOSHeaderData, 0, res, 0, DOSHeaderData.Length);
            Array.Copy(DOSStub, 0, res, DOSHeaderData.Length, DOSStub.Length);
            Array.Copy(COFFHeaderData, 0, res, DOSHeaderData.Length + DOSStub.Length, COFFHeaderData.Length);
            Array.Copy(OptionalHeaderData, 0, res, DOSHeaderData.Length + DOSStub.Length + COFFHeaderData.Length, OptionalHeaderData.Length);
            for (int i = 0; i < SectionTable.Length; i++)
            {
                int startIndex = DOSHeaderData.Length + DOSStub.Length + COFFHeaderData.Length + OptionalHeaderData.Length + sectionLength * i;
                Array.Copy(SectionTable[i].GetBytes(), 0, res, startIndex, sectionLength);
                Array.Copy(SectionRawData[i], 0, res, SectionTable[i].PointerToRawData, SectionTable[i].SizeOfRawData);
            }
            return res;
        }
    }
}
